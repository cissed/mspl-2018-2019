---
title: 'TD3: Ping Pong'
output:
  pdf_document: default
  word_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
# Introduction

# Exemple de lecture des données
```{r}
df <- read.csv("data/PP_size_3.csv", header=TRUE, col.names=c("time"))
head(df);
```
# Conctenation des données du repertoire data
```{r}
df1 <- read.csv("data/PP_size_1.csv", header=TRUE, col.names=c("time"));
df2 <- read.csv("data/PP_size_2.csv", header=TRUE, col.names=c("time"));
df3 <- read.csv("data/PP_size_3.csv", header=TRUE, col.names=c("time"));
df4 <- read.csv("data/PP_size_4.csv", header=TRUE, col.names=c("time"));
df5 <- read.csv("data/PP_size_5.csv", header=TRUE, col.names=c("time"));
df <- rbind(df1, df2, df3, df4, df5);
```

# Vue d'ensemble des mesures

```{r, fig.width=6, fig.height=3.5}
plot(df$time, ylab="Time (seconds)", xlab="Measurement Number")
```

# Resumé statistique
```{r}
summary(df$time)
```

# Boxplot
```{r, fig.width=2, fig.height=3.5}
boxplot(df$time, ylab="Time (seconds)")
```

## Histogramme
```{r, fig.width=6}
hist(df$time, breaks=10, xlab="Time (seconds)", main="Histogram of Ping-Pong")
```

## Variabilité
```{r}
sd(df$time)
```

## Wrap up
```{r}
mean(df$time)
sd(df$time)
```